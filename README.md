# Vim Workshop

## Preparation

To prepare for this workshop follow these steps:

1. [Install vim 7.4 or 8.0](https://www.vim.org/download.php)
2. Clone this repository
3. Configure your vim
    1. For linux copy the contents of repo:/config to your home folder /home/your_username/
    2. For windows copy the contents of repo:/config to C:\Users\your_username\

## Original Creators and license

This workshop is based on the one created by Alexander Swen and Joel Stemmer (see [github](https://github.com/nedap/vim-workshop)) and therefore published under the same licence.

<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="http://i.creativecommons.org/l/by-sa/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Vim workshop</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/aswen/vim-workshop" property="cc:attributionName" rel="cc:attributionURL">Alexander Swen</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://github.com/nedap/vim-workshop" rel="dct:source">https://github.com/nedap/vim-workshop</a>. It was created by Alexander Swen & Joël Stemmer for Nedap N.V..
