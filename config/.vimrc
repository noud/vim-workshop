source ~/vim/bundles.vim

set hlsearch
set ignorecase
set number
set hidden
set cursorline
set showtabline=2  " 0, 1 or 2; when to use a tab pages line

set ts=4 sts=4 sw=4 expandtab
set tags+=~/tags

if has('mouse')
  set mouse=a
endif

" Source the vimrc file after saving it
if has("autocmd")
  autocmd bufwritepost .vimrc source $MYVIMRC
endif
nnoremap <leader>v :tabedit $MYVIMRC<CR>

" Get rid of the arrow keys for training
"noremap <Up> <Nop>
"noremap <Down> <Nop>
"noremap <Left> <Nop>
"noremap <Right> <Nop>

set path+=**
