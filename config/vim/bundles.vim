set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('C:\Localdata\Vim\vimconfig\bundle')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" My plugins                                               """
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" General enhancements
Plugin 'tpope/vim-sensible'
Plugin 'tpope/vim-unimpaired.git'
Plugin 'vim-airline/vim-airline'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" End of my plugins                                        """
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
